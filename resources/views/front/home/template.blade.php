<!doctype html>
<html lang="{{ app()->getLocale() }}">

<!-- Mirrored from simpleqode.bitbucket.io/touche/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Aug 2018 23:17:19 GMT -->
<head>
    <style type="text/css">.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}
    </style>
    <style type="text/css">.gm-control-active img{box-sizing:content-box}.gm-control-active:hover img:nth-child(1),.gm-control-active:active img:nth-child(1),.gm-control-active:active img:nth-child(2){display:none}
    </style>
    <style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px;box-sizing:border-box}
    </style>
    <style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style>
    <style type="text/css">.gm-style .gm-style-iw{font-weight:300;font-size:13px;overflow:hidden}.gm-style .gm-iw{color:#2c2c2c}.gm-style .gm-iw b{font-weight:400}.gm-style .gm-iw a:link,.gm-style .gm-iw a:visited{color:#4272db;text-decoration:none}.gm-style .gm-iw a:hover{color:#4272db;text-decoration:underline}.gm-style .gm-iw .gm-title{font-weight:400;margin-bottom:1px}.gm-style .gm-iw .gm-basicinfo{line-height:18px;padding-bottom:12px}.gm-style .gm-iw .gm-website{padding-top:6px}.gm-style .gm-iw .gm-photos{padding-bottom:8px;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none}.gm-style .gm-iw .gm-sv,.gm-style .gm-iw .gm-ph{cursor:pointer;height:50px;width:100px;position:relative;overflow:hidden}.gm-style .gm-iw .gm-sv{padding-right:4px}.gm-style .gm-iw .gm-wsv{cursor:pointer;position:relative;overflow:hidden}.gm-style .gm-iw .gm-sv-label,.gm-style .gm-iw .gm-ph-label{cursor:pointer;position:absolute;bottom:6px;color:#fff;font-weight:400;text-shadow:rgba(0,0,0,0.7) 0 1px 4px;font-size:12px}.gm-style .gm-iw .gm-stars-b,.gm-style .gm-iw .gm-stars-f{height:13px;font-size:0}.gm-style .gm-iw .gm-stars-b{position:relative;background-position:0 0;width:65px;top:3px;margin:0 5px}.gm-style .gm-iw .gm-rev{line-height:20px;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none}.gm-style.gm-china .gm-iw .gm-rev{display:none}.gm-style .gm-iw .gm-numeric-rev{font-size:16px;color:#dd4b39;font-weight:400}.gm-style .gm-iw.gm-transit{margin-left:15px}.gm-style .gm-iw.gm-transit td{vertical-align:top}.gm-style .gm-iw.gm-transit .gm-time{white-space:nowrap;color:#676767;font-weight:bold}.gm-style .gm-iw.gm-transit img{width:15px;height:15px;margin:1px 5px 0 -20px;float:left}
        .gm-iw {text-align:left;}.gm-iw .gm-numeric-rev {float:left;}.gm-iw .gm-photos,.gm-iw .gm-rev {direction:ltr;}.gm-iw .gm-stars-f, .gm-iw .gm-stars-b {background:url("https://maps.gstatic.com/mapfiles/api-3/images/review_stars.png") no-repeat;background-size: 65px 26px;float:left;}.gm-iw .gm-stars-f {background-position:left -13px;}.gm-iw .gm-sv-label,.gm-iw .gm-ph-label {left: 4px;}</style>
    <style type="text/css">.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('front/ico/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('front/ico/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('front/ico/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('front/ico/manifest.json')}}">
    <link rel="mask-icon"href="{{asset('front/ico/safari-pinned-tab.svg')}}" color="#5bbad5">
    <link rel="shortcut icon" href="{{asset('front/ico/favicon.ico')}}">
    <meta name="msapplication-config" content="{{asset('front/ico/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">

    <title>Reservada</title>

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="{{asset('front/plugins/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/plugins/lightbox/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/plugins/flickity/flickity.min.css')}}">

    <!-- CSS Global -->
    <link rel="stylesheet" href="{{asset('front/css/theme.min.css')}}">
    <style>@font-face {
            font-family: 'Omnes';
            font-style: normal;
            font-weight: 500;
            src: url('chrome-extension://bgkknpjjkdiodnjnkkabgnkfdhcokden/fonts/Omnes-Regular.otf');
        }
        @font-face {
            font-family: 'Omnes';
            font-style: bold;
            font-weight: 700;
            src: url('chrome-extension://bgkknpjjkdiodnjnkkabgnkfdhcokden/fonts/Omnes-Semibold.otf');
        }
    </style>
    <style type="text/css">.gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }
        .gm-style img { max-width: none; }</style>
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
<body>
@yield('test')
<!-- JAVASCRIPT
================================================== -->

<!-- JS Global -->
<script src="{{asset('front/js/jquery.min.js')}}"></script>
<script src="{{asset('front/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- JS Plugins -->
<script src="{{asset('front/plugins/parallax/parallax.min.js')}}"></script>
<script src="{{asset('front/plugins/isotope/lib/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('front/plugins/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('front/plugins/flickity/flickity.pkgd.min.js')}}"></script>
<script src="{{asset('front/plugins/lightbox/js/lightbox.min.js')}}"></script>
<script src="{{asset('front/plugins/reservation/reservation.js')}}"></script>
<script src="{{asset('front/plugins/alerts/alerts.js')}}"></script>

<!-- JS Custom -->
<script src="{{asset('front/js/theme.min.js')}}"></script>
<script src="{{asset('front/js/custom.js')}}"></script>

</body>

<!-- Mirrored from simpleqode.bitbucket.io/touche/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Aug 2018 23:17:35 GMT -->
</html>