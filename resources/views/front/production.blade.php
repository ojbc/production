@extends('front.home.template')
@section('test')
    <div class="parallax-mirror" style="visibility: visible; z-index: -100; position: fixed; top: 0px; left: 0px; overflow: hidden; transform: translate3d(0px, 0px, 0px); height: 943px; width: 1903px;"><img class="parallax-slider" src="{{asset('front/img/constr1.jpg')}}" style="transform: translate3d(0px, 0px, 0px); position: absolute; top: -162px; left: 0px; height: 1268px; width: 1903px; max-width: none;"></div>
    <!-- NAVBAR
    ================================================== -->
    <nav class="navbar navbar-light navbar-expand-lg fixed-top">
        <div class="container">

            <!-- Navbar: Brand -->
            <a class="navbar-brand d-lg-none" href="#section_welcome">Reserveda</a>

            <!-- Navbar: Toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar: Collapse -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Navbar navigation: Left -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#section_welcome">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section_reservation">Reservation</a>
                    </li>
                </ul>

                <!-- Navbar: Brand -->
                <a class="navbar-brand d-none d-lg-flex" href="#section_welcome">
                    Reservada
                </a>

                <!-- Navbar navigation: Right -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#section_events">News & Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section_gallery">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"  href="#section_footer">Contact</a>
                    </li>
                </ul>

            </div> <!-- / .navbar-collapse -->

        </div> <!-- / .container -->
    </nav> <!-- / .navbar -->

    <!-- WELCOME
    ================================================== -->
    <section class="section section_welcome section_gray" id="section_welcome" data-parallax="scroll" data-image-src="{{asset('front/img/slide-1.jpg')}}">

        <!-- Content -->
        <div class="section_welcome__main ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col">

                    <!-- Heading -->
                    <h2 class="section__preheading section_welcome__preheading text-center text-muted">
                        <span class="text-primary">Constructora</span>
                    </h2>
                    <h1 class="section__heading section_welcome__heading text-center">
                        Reservada
                    </h1>
                    <p class="section__subheading section_welcome__subheading text-center text-muted">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non illo, alias animi iusto neque, sint corrupti? Laudantium, dignissimos id excepturi facilis, facere saepe quasi placeat praesentium ipsa sapiente illo molestiae?
                    </p>

                    <!-- Button -->
                    <div class="text-center">
                        <a href="#section_reservation" class="btn btn-primary text-white">
                            Make reservation
                        </a>
                    </div>

                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </div>

    <!-- Footer -->
    <div class="section_welcome__footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md">

                    <!-- Social links -->
                    <ul class="section_welcome__footer__social text-center text-lg-left">
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-md">

                    <!-- Address -->
                    <div class="section_welcome__footer__address text-center text-lg-right">
                        <i class="fa fa-map-marker"></i> Venezuela
                    </div>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </div>

</section>

<!-- DISCOVER
================================================== -->
<section class="section section_discover section_no-padding_bottom section_white">
    <div class="container">
        <div class="row">
            <div class="col-md-2 align-self-start">

                <!-- Description -->
                <p class="section_discover__description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>

            </div>
            <div class="col-md-4 align-self-start">

                <!-- Image -->
                <div class="section_discover__img">
                    <img src="{{asset('front/img/2.jpg')}}" class="img-fluid" alt="...">
                </div>

            </div>
            <div class="col-md-6 align-self-center">

                <!-- Content -->
                <h2><em>Lorem ipsum</em> dolor sit amet</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias veritatis nisi, consequatur, laborum libero a neque ducimus. Porro rem illum quo nostrum quisquam asperiores, blanditiis, consectetur. Possimus facilis velit, voluptatibus!
                </p>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- DISCOVER
================================================== -->
<section class="section section_discover section_white">
    <div class="container">
        <div class="row">
            <div class="col-md-2 order-md-2 align-self-end">

                <!-- Description -->
                <p class="section_discover__description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>

            </div>
            <div class="col-md-4 order-md-3 align-self-start">

                <!-- Image -->
                <div class="section_discover__img alt">
                    <img src="{{asset('front/img/7.jpg')}}" class="img-fluid" alt="...">
                </div>

            </div>
            <div class="col-md-6 order-md-1 align-self-center">

                <!-- Content -->
                <h2><em>Velit ipsa</em> quidem debitis amet</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quae similique asperiores consequatur! Nihil temporibus qui enim, ab voluptates corporis commodi eum maxime. Accusamus voluptates a, et quidem! Quo, et?
                </p>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- ABOUT
================================================== -->
<section class="section section_about section_gray">
    <div class="container">
        <div class="row">
            <div class="col-md-4">

                <!-- Description -->
                <p class="section_about__description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam placeat molestiae ab, consectetur.
                </p>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- MENU
================================================== -->
<section class="section section_menu section_border_bottom section_white">
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading text-center">
                    Our menu
                </h2>
                <p class="section__subheading text-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Ratione numquam eos perferendis itaque hic unde, ad, laudantium minima.
                </p>

            </div>
        </div> <!-- / .row -->
        <div class="row">
            <div class="col">

                <!-- Navigation -->
                <nav class="section_menu__nav">
                    <ul>
                        <li class="active">
                            <a href="#menu_images" data-filter=".mains">Mains</a>
                        </li>
                        <li>
                            <a href="#menu_images" data-filter=".lunch">Lunch</a>
                        </li>
                        <li>
                            <a href="#menu_images" data-filter=".dinner">Dinner</a>
                        </li>
                        <li>
                            <a href="#menu_images" data-filter=".drinks">Drinks</a>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>
        <div class="row section_menu__grid" id="menu_images">

            <div class="col-md-6 section_menu__grid__item mains">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/26.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $15
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item lunch">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/31.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce id ante ut arcu</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $23
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item dinner">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/27.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce sed dolor eget tortor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $16
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item drinks">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/28.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Phasellus non elit in dolor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $10
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item drinks">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/29.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>In vel odio eu massa semper</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $35
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item dinner">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/28.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Morbi ac est consectetur</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $18
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item lunch">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/30.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $15
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item mains">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/27.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce id ante ut arcu</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $23
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item mains">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/28.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce sed dolor eget tortor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $16
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item lunch">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/29.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Phasellus non elit in dolor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $10
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item dinner">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/29.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>In vel odio eu massa semper</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $35
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item drinks">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/30.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Morbi ac est consectetur</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $18
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item mains">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/29.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $15
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item lunch">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/28.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce id ante ut arcu</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $23
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item dinner">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/30.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce sed dolor eget tortor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $16
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item drinks">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/31.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Phasellus non elit in dolor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $10
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item drinks">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/26.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>In vel odio eu massa semper</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $35
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item dinner">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/31.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Morbi ac est consectetur</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $18
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item lunch">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/27.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Lorem ipsum dolor sit amet</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $15
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item mains">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/30.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce id ante ut arcu</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $23
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item mains">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/31.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Fusce sed dolor eget tortor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $16
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item lunch">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/26.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Phasellus non elit in dolor</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $10
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item dinner">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/26.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>In vel odio eu massa semper</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $35
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            <div class="col-md-6 section_menu__grid__item drinks">
                <div class="section_menu__item">
                    <div class="row">
                        <div class="col-3 align-self-center">
                            <div class="section_menu__item__img">
                                <img src="{{asset('front/img/27.jpg')}}" alt="...">
                            </div>
                        </div>
                        <div class="col-7">
                            <h4>Morbi ac est consectetur</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, obcaecati! Sapiente laudantium.
                            </p>
                        </div>
                        <div class="col-2">
                            <div class="section_menu__item__price text-center">
                                $18
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>

        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- DISHES
================================================== -->
<section class="section section_dishes section_white">

    <!-- Header -->
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading text-center">
                    Featured dishes
                </h2>

                <!-- Subheading -->
                <p class="section__subheading text-center">
                    Quibusdam in labore tempore quidem voluptatum ullam soluta! Maiores!
                </p>

            </div>
        </div>
    </div>

    <!-- Carousel -->
    <div class="section_dishes__carousel dots-disabled">
        <div class="section_dishes__carousel__item">

            <!-- Image -->
            <img src="{{asset('front/img/26.jpg')}}" alt="..." class="section_dishes__carousel__item__img">

            <!-- Body -->
            <div class="section_dishes__carousel__item__body">
                <h5 class="section_dishes__carousel__item__body__heading mb-0">
                    Lorem ipsum dolor sit amet <span>$25</span>
                </h5>
            </div>

        </div>
        <div class="section_dishes__carousel__item">

            <!-- Image -->
            <img src="{{asset('front/img/27.jpg')}}" alt="..." class="section_dishes__carousel__item__img">

            <!-- Body -->
            <div class="section_dishes__carousel__item__body">
                <h5 class="section_dishes__carousel__item__body__heading mb-0">
                    Lorem ipsum dolor sit amet <span>$35</span>
                </h5>
            </div>

        </div>
        <div class="section_dishes__carousel__item">

            <!-- Image -->
            <img src="{{asset('front/img/28.jpg')}}" alt="..." class="section_dishes__carousel__item__img">

            <!-- Body -->
            <div class="section_dishes__carousel__item__body">
                <h5 class="section_dishes__carousel__item__body__heading mb-0">
                    Lorem ipsum dolor sit amet <span>$18</span>
                </h5>
            </div>

        </div>
        <div class="section_dishes__carousel__item">

            <!-- Image -->
            <img src="{{asset('front/img/29.jpg')}}" alt="..." class="section_dishes__carousel__item__img">

            <!-- Body -->
            <div class="section_dishes__carousel__item__body">
                <h5 class="section_dishes__carousel__item__body__heading mb-0">
                    Lorem ipsum dolor sit amet <span>$32</span>
                </h5>
            </div>

        </div>
        <div class="section_dishes__carousel__item">

            <!-- Image -->
            <img src="{{asset('front/img/30.jpg')}}" alt="..." class="section_dishes__carousel__item__img">

            <!-- Body -->
            <div class="section_dishes__carousel__item__body">
                <h5 class="section_dishes__carousel__item__body__heading mb-0">
                    Lorem ipsum dolor sit amet <span>$40</span>
                </h5>
            </div>

        </div>
        <div class="section_dishes__carousel__item">

            <!-- Image -->
            <img src="{{asset('front/img/31.jpg')}}" alt="..." class="section_dishes__carousel__item__img">

            <!-- Body -->
            <div class="section_dishes__carousel__item__body">
                <h5 class="section_dishes__carousel__item__body__heading mb-0">
                    Lorem ipsum dolor sit amet <span>$27</span>
                </h5>
            </div>

        </div>
    </div> <!-- / .section_dishes__carousel -->

</section>

<!-- RESERVATION
================================================== -->
<section class="section section_reservation section_gray" id="section_reservation">
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading text-center">
                    Make online reservation
                </h2>

                <!-- Subheading -->
                <p class="section__subheading text-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam illo praesentium sequi in cum, beatae maiores quae qui.
                </p>

            </div>
        </div> <!-- / .row -->
        <div class="row justify-content-lg-center  section_reservation__row">
            <div class="col-lg-8">

                <!-- Form -->
                <form class="section_reservation__form" id="reservation__form">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="sr-only" for="reservation__form__name">Full name</label>
                                <input type="text" class="form-control" id="reservation__form__name" name="reservation__form__name" placeholder="Full name">
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="reservation__form__phone">Phone number</label>
                                <input type="tel" class="form-control" id="reservation__form__phone" name="reservation__form__phone" placeholder="Phone number">
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="reservation__form__email">E-mail address</label>
                                <input type="email" class="form-control" id="reservation__form__email" name="reservation__form__email" placeholder="E-mail address">
                                <div class="invalid-feedback"></div>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="sr-only" for="reservation__form__people">People</label>
                                <select class="form-control" id="reservation__form__people" name="reservation__form__people">
                                    <option value="1">1 person</option>
                                    <option value="2" selected="">2 persons</option>
                                    <option value="3">3 persons</option>
                                    <option value="4">4 persons</option>
                                    <option value="5">5 persons</option>
                                </select>
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="reservation__form__date">Date</label>
                                <input type="date" class="form-control" id="reservation__form__date" name="reservation__form__date" value="2016-12-31">
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="reservation__form__time">Time</label>
                                <input type="time" class="form-control" id="reservation__form__time" name="reservation__form__time" value="18:00">
                                <div class="invalid-feedback"></div>
                            </div>

                        </div>
                        <div class="col">

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">
                                    Reserve a table
                                </button>
                            </div>

                        </div>
                    </div> <!-- / .row -->
                </form>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- TESTIMONIALS
================================================== -->
<section class="section section_testimonials section_border_bottom section_white">
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading text-center">
                    What clients say about us
                </h2>

                <!-- Subheading -->
                <p class="section__subheading text-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>

            </div>
        </div> <!-- / .row -->
        <div class="row">
            <div class="col">
                <div class="section_testimonials__carousel">
                    <div class="section_testimonials__carousel__item text-center text-md-left">
                        <div class="row align-items-center">
                            <div class="col-md-3 order-md-3">

                                <!-- Photo -->
                                <div class="section_testimonials__photo">
                                    <img src="{{asset('front/img/20.jpg')}}" class="img-fluid" alt="...">
                                </div>

                            </div>
                            <div class="col-md-7 order-md-2">

                                <!-- Blockquote -->
                                <blockquote class="section_testimonials__blockquote mx-auto">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos at veritatis vitae modi ex quis quibusdam error repudiandae adipisci dolore perspiciatis iste, vel fuga a, libero architecto ratione deleniti sequi.
                                    </p>
                                    <footer class="text-muted">
                                        Richard Roe
                                    </footer>
                                </blockquote>

                            </div>
                            <div class="col-md-1 order-md-1"></div>
                        </div> <!-- / .row -->
                    </div>
                    <div class="section_testimonials__carousel__item text-center text-md-left">
                        <div class="row align-items-center">
                            <div class="col-md-3 order-md-3">

                                <!-- Photo -->
                                <div class="section_testimonials__photo">
                                    <img src="{{asset('front/img/21.jpg')}}" class="img-fluid" alt="...">
                                </div>

                            </div>
                            <div class="col-md-7 order-md-2">

                                <!-- Blockquote -->
                                <blockquote class="section_testimonials__blockquote mx-auto">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos at veritatis vitae modi ex quis quibusdam error repudiandae adipisci dolore perspiciatis iste, vel fuga a, libero architecto ratione deleniti sequi.
                                    </p>
                                    <footer class="text-muted">
                                        Elisabeth Doe
                                    </footer>
                                </blockquote>

                            </div>
                            <div class="col-md-1 order-md-1"></div>
                        </div> <!-- / .row -->
                    </div>
                    <div class="section_testimonials__carousel__item text-center text-md-left">
                        <div class="row align-items-center">
                            <div class="col-md-3 order-md-3">

                                <!-- Photo -->
                                <div class="section_testimonials__photo">
                                    <img src="{{asset('front/img/22.jpg')}}" class="img-fluid" alt="...">
                                </div>

                            </div>
                            <div class="col-md-7 order-md-2">

                                <!-- Blockquote -->
                                <blockquote class="section_testimonials__blockquote mx-auto">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos at veritatis vitae modi ex quis quibusdam error repudiandae adipisci dolore perspiciatis iste, vel fuga a, libero architecto ratione deleniti sequi.
                                    </p>
                                    <footer class="text-muted">
                                        Monica Roe
                                    </footer>
                                </blockquote>

                            </div>
                            <div class="col-md-1 order-md-1"></div>
                        </div> <!-- / .row -->
                    </div>
                </div> <!-- / .carousel -->
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- EVENTS
================================================== -->
<section class="section section_events section_no-padding_bottom section_white" id="section_events">

    <!-- Intro -->
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading text-center">
                    Upcoming events
                </h2>

                <!-- Subheading -->
                <p class="section__subheading text-center">
                    Commodi rem veritatis quis eaque mollitia.
                </p>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->

    <!-- Events -->
    <div class="section_events__items">

        <!-- Event #1 -->
        <div class="section_events__item active">

            <!-- Bg image -->
            <div class="section_events__item__bg" style="background-image: url({{asset('front/img/constr2.jpg')}})"></div>

            <!-- Content -->
            <div class="container">

                <!-- Content: Large -->
                <div class="section_events__item__content_lg">
                    <div class="row">
                        <div class="col-md-8 col-lg-5">

                            <!-- Date -->
                            <h3 class="section__preheading text-primary">
                                <time datetime="2017-09-30">September 30, 2017</time>
                            </h3>

                            <!-- Heading -->
                            <h2 class="section__heading text-white">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum voluptas, aut ea quae!
                            </h2>

                            <!-- Description -->
                            <p class="section__subheading">
                                Alias earum, labore doloremque iusto a modi et, dolorem veritatis iste quis ab. Facere est optio, voluptate molestias aspernatur impedit perferendis odit?
                            </p>

                            <!-- Button -->
                            <a href="#section_reservation" class="btn btn-primary text-white">
                                Reserve a table
                            </a>

                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .section_events__item__content_lg -->

                <!-- Content: Small -->
                <div class="section_events__item__content_sm ">
                    <div class="row">
                        <div class="col-3 col-md-2 col-lg-1">

                            <!-- Date -->
                            <time datetime="2017-09-30">
                                30 <small>Sep</small>
                            </time>

                        </div>
                        <div class="col-9 col-md-7 col-lg-8">

                            <!-- Heading -->
                            <h4>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum voluptas, aut ea quae!
                            </h4>

                        </div>
                        <div class="col-9 ml-auto col-md-3 text-md-right">

                            <!-- Button -->
                            <a href="#section_reservation" class="btn btn-primary">
                                Reserve a table
                            </a>

                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .section_events__item__content_sm -->

            </div> <!-- / .container -->

        </div> <!-- / .section_events__item -->

        <!-- Event #2 -->
        <div class="section_events__item">

            <!-- Bg image -->
            <div class="section_events__item__bg" style="background-image: url({{asset('front/img/9.jpg')}})"></div>

            <!-- Content -->
            <div class="container">

                <!-- Content: Large -->
                <div class="section_events__item__content_lg">
                    <div class="row">
                        <div class="col-md-8 col-lg-5">

                            <!-- Date -->
                            <h3 class="section__preheading text-primary">
                                <time datetime="2017-09-29">September 29, 2017</time>
                            </h3>

                            <!-- Heading -->
                            <h2 class="section__heading text-white">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum voluptas, aut ea quae!
                            </h2>

                            <!-- Description -->
                            <p class="section__subheading">
                                Alias earum, labore doloremque iusto a modi et, dolorem veritatis iste quis ab. Facere est optio, voluptate molestias aspernatur impedit perferendis odit?
                            </p>

                            <!-- Button -->
                            <a href="#section_reservation" class="btn btn-primary text-white">
                                Reserve a table
                            </a>

                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .section_events__item__content_lg -->

                <!-- Content: Small -->
                <div class="section_events__item__content_sm">
                    <div class="row">
                        <div class="col-3 col-md-2 col-lg-1">

                            <!-- Date -->
                            <time datetime="2017-09-29">
                                29 <small>Sep</small>
                            </time>

                        </div>
                        <div class="col-9 col-md-7 col-lg-8">

                            <!-- Heading -->
                            <h4>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum voluptas, aut ea quae!
                            </h4>

                        </div>
                        <div class="col-9 ml-auto col-md-3 text-md-right">

                            <!-- Button -->
                            <a href="#section_reservation" class="btn btn-primary">
                                Reserve a table
                            </a>

                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .section_events__item__content_sm -->

            </div> <!-- / .container -->

        </div> <!-- / .section_events__item -->

        <!-- Event #3 -->
        <div class="section_events__item">

            <!-- Bg image -->
            <div class="section_events__item__bg" style="background-image: url({{asset('front/img/10.jpg')}})"></div>

            <!-- Content -->
            <div class="container">

                <!-- Content: Large -->
                <div class="section_events__item__content_lg">
                    <div class="row">
                        <div class="col-md-8 col-lg-5">

                            <!-- Date -->
                            <h3 class="section__preheading text-primary">
                                <time datetime="2017-09-28">September 28, 2017</time>
                            </h3>

                            <!-- Heading -->
                            <h2 class="section__heading text-white">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum voluptas, aut ea quae!
                            </h2>

                            <!-- Description -->
                            <p class="section__subheading">
                                Alias earum, labore doloremque iusto a modi et, dolorem veritatis iste quis ab. Facere est optio, voluptate molestias aspernatur impedit perferendis odit?
                            </p>

                            <!-- Button -->
                            <a href="#section_reservation" class="btn btn-primary text-white">
                                Reserve a table
                            </a>

                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .section_events__item__content_lg -->

                <!-- Content: Small -->
                <div class="section_events__item__content_sm">
                    <div class="row">
                        <div class="col-3 col-md-2 col-lg-1">

                            <!-- Date -->
                            <time datetime="2017-09-28">
                                28 <small>Sep</small>
                            </time>

                        </div>
                        <div class="col-9 col-md-7 col-lg-8">

                            <!-- Heading -->
                            <h4>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum voluptas, aut ea quae!
                            </h4>

                        </div>
                        <div class="col-9 ml-auto col-md-3 text-md-right">

                            <!-- Button -->
                            <a href="#section_reservation" class="btn btn-primary">
                                Reserve a table
                            </a>

                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .section_events__item__content_sm -->

            </div> <!-- / .container -->

        </div> <!-- / .section_events__item -->

    </div> <!-- / .section_events_items -->

</section>

<!-- NEWSLETTER
================================================== -->
<section class="section section_gray section_newsletter">
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading section_newsletter__heading text-center">
                    Newsletter
                </h2>
                <p class="section__subheading text-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam illo praesentium sequi in cum, beatae maiores quae qui.
                </p>

            </div>
        </div> <!-- / .row -->
        <div class="row justify-content-center">
            <div class="col-lg-6">

                <!-- Form -->
                <div id="mc_embed_signup">
                    <form class="section_newsletter__form validate" action="http://simpleqode.us15.list-manage.com/subscribe/post-json?u=507744bbfd1cc2879036c7780&amp;id=4523d25e1b&amp;c=?" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll" class="row">
                            <div class="mc-field-group form-group col-md-9">
                                <label for="mce-EMAIL" class="sr-only">E-mail address</label>
                                <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Email address">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div aria-hidden="true" id="mce-hidden-input">
                                <input type="text" name="b_507744bbfd1cc2879036c7780_4523d25e1b" tabindex="-1" value="">
                            </div>
                            <div class="clear col-md-3 text-center">
                                <button type="submit" class="btn btn-primary" id="mc-embedded-subscribe">
                                    Subscribe
                                </button>
                            </div>
                        </div>
                    </form>
                </div> <!-- #mc_embed_signup -->

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- NEWSLETTER CLONE
================================================== -->
<!--
    MailChimp newsletters generally cannot be used more than
    once on a page, because this will cause HTML IDs conflict.
    This section solves this problem by calling the
    original sign-up form and imitating its behaviour. Please
    don't use it without the original sign-up form. Remove
    d-none to make it visible.
-->
<section class="section section_gray section_newsletter d-none">
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading section_newsletter__heading text-center">
                    Newsletter
                </h2>
                <p class="section__subheading text-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam illo praesentium sequi in cum, beatae maiores quae qui.
                </p>

            </div>
        </div> <!-- / .row -->
        <div class="row justify-content-center">
            <div class="col-lg-6">

                <!-- Form -->
                <form class="section_newsletter__form section_newsletter__form_clone">
                    <div class="row">
                        <div class="form-group col-md-9">
                            <label class="sr-only">E-mail address</label>
                            <input type="email" class="form-control" placeholder="Email address">
                        </div>
                        <div class="col-md-3 text-center">
                            <button type="submit" class="btn btn-primary">
                                Subscribe
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<!-- GALLERY
================================================== -->
<section class="section section_gallery section_white" id="section_gallery">
    <div class="container">
        <div class="row">
            <div class="col">

                <!-- Heading -->
                <h2 class="section__heading text-center">
                    Our gallery
                </h2>

                <!-- Subheading -->
                <p class="section__subheading text-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>

            </div>
        </div> <!-- / .row -->
        <div class="row section_gallery__grid">
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/11.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/11.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/16.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/16.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/13.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/13.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/15.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/15.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/14.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/14.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/18.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/18.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/17.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/17.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
            <div class="col-6 col-sm-6 col-md-4 section_gallery__grid__item">

                <a href="{{asset('front/img/12.jpg')}}" data-lightbox="gallery">
                    <img src="{{asset('front/img/12.jpg')}}" class="img-fluid" alt="...">
                </a>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

<footer class="section section_footer" id="section_footer" >
    <div class="container">
        <div class="row">
            <div class="col-sm-4">

                <!-- About Us -->
                <h5 class="section_footer__heading">
                    About Us
                </h5>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti dolorum, sint corporis nostrum, possimus unde eos vitae eius quasi saepe.
                </p>

            </div>
            <div class="col-sm-4">

                <!-- Contact info -->
                <h5 class="section_footer__heading">
                    Contact info
                </h5>
                <ul class="section_footer__info">
                    <li>
                        <i class="fa fa-map-marker"></i> 1234 Altschul, New York, NY 10027-0000
                    </li>
                    <li>
                        <i class="fa fa-phone"></i> +1 987 654 3210
                    </li>
                    <li>
                        <i class="fa fa-envelope-o"></i> <a href="mailto:admin@domain.com">admin@domain.com</a>
                    </li>
                </ul>

                    <h4 class="section_footer__heading">
                        Social links
                    </h4>
                    <ul class="section_contact__info__item__content">
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>


            </div>
            <div class="col-sm-4">

                <!-- Opening hours -->
                <h5 class="section_footer__heading">
                    Opening hours
                </h5>
                <div class="section_footer__open">
                    <div class="section_footer__open__days">Monday - Thursday</div>
                    <div class="section_footer__open__time">10:00 AM - 11:00 PM</div>
                </div>
                <div class="section_footer__open">
                    <div class="section_footer__open__days">Friday - Sunday</div>
                    <div class="section_footer__open__time">12:00 AM - 03:00 AM</div>
                </div>

            </div>
        </div> <!-- / .row -->
        <div class="row">
            <div class="col-12">

                <!-- Copyright -->
                <div class="section_footer__copyright">
                    <i class="fa fa-copyright"></i> <span id="js-current-year"></span> Touché. Orlado Bravo and Carlos Hurtado reserved.
                </div>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</footer>
@endsection