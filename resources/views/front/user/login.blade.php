@extends('front.template.main')
@section('content')
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form action="{{ route('test.login.post') }}" method="post">
                <div class="form-group">
                    <div class="form-label-group">
                        <input class="form-control" type="email" placeholder="Email" name="email" id="email" title="Email">
                        <label for="email">Email address</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input  class="form-control" id="password" type="password" placeholder="Password" title="Password" name="password">
                        <label for="password">Password</label>
                    </div>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Login</button>
            </form>
        </div>
    </div>
@endsection