<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function login(Request $request)// función para loguearse
    {
        \Log::debug('$request', [$request]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password

        ];
        if (auth()->attempt($credentials)) {

            return redirect()->route('test.home');
        }
    }
    public function logout()
    {
        auth()->logout();
        return redirect()->route('test.login');
    }
    public function viewUser()
{
    return view('front.production');
}
    public function viewLogin()
    {
        return view('front.user.login');
    }
    public function viewHomes()
    {
        return view('front.user.homes');

    }
}
