<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('', [
    'as' => 'test.user',
    'uses' => 'UserController@viewUser'
]);

Route::get('login', [
    'as' => 'test.login',
    'uses' => 'UserController@viewLogin'
]);

Route::post('login', [
    'as' => 'test.login.post',
    'uses' => 'UserController@login'
]);
Route::get('/home', [
    'middleware' => ['auth'],
    'as' => 'rapipagos.home',
    'uses' => 'UserController@viewHomes'
]);

